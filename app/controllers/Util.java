package controllers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;


public class Util {
    public static Map<String, Person> family = new HashMap<>();
    
    public Util(String path) throws Exception { 
    	parseFamily(path);
    	populateSiblings();
    }
    
    public void parseFamily(String path) throws Exception {
        BufferedReader in = null;
        
        try {
            if (path == null)
                throw new NullPointerException();

            in = new BufferedReader(new FileReader(path));

            String inputLine = "";
            while ((inputLine = in.readLine()) != null) {

                // Trim, split on space and add to map
                String[] tokens = inputLine.trim().split(" ");

                // Create person objects
                if (tokens.length == 5) {
                    String name = tokens[0]; 
                    char gender = tokens[1].charAt(0);
                    String dob = tokens[2];
                    String mothersName = tokens[3];
                    String fathersName = tokens[4];
                    
                    Person mother = null;
                    Person father = null;
                    
                    // Add the mother node
                    if(!mothersName.equals("?")) {
                    	if(!family.containsKey(mothersName)) {
                    		mother = new Person(mothersName, 'F', "", null, null);
                    		this.family.put(mothersName, mother);
                    	}
                    	else {
                    		mother = this.family.get(mothersName);
                    	}
                    }

                    // Add the father node
                    if(!fathersName.equals("?")) {
                    	if(!family.containsKey(fathersName)) {
                    		father = new Person(fathersName, 'M', "", null, null);
                    		this.family.put(fathersName, father);
                    	}
                    	else {
                    		father = this.family.get(fathersName);
                    	}
                    }
                    
                    Person person = null;
                    // Add the 'person'
                    if(family.containsKey(name)) {
                    	person = this.family.get(name);
                    	person.setDob(dob);
                    	person.setGender(gender);
                    	person.setMother(mother);
                    	person.setFather(father);
                    }
                    else {
                    	person = new Person(name, gender, dob, mother, father);
                    	this.family.put(name, person);
                    }
                    
                    if(father != null) {
                    	father.addChild(person);
                    }
                    if(mother != null) {
                    	mother.addChild(person);
                    }
                    if(mother != null && father != null) {
                    	mother.setSpouse(father);
                    	father.setSpouse(mother);
                    }
                } 
                else {
                    in.close();
                    throw new IOException("Invalid member length: " + tokens.length);
                }
            }
        } finally {
            if (in != null)
                in.close();
        }
    }

    private void populateSiblings() {
    	for(Map.Entry<String, Person> person : family.entrySet()) {
    		if(person.getValue().getMother() != null) {
    			Set<Person> siblings = person.getValue().getMother().getChildren();
    			
    			for(Person sib : siblings) {
    				if (sib.getName() != person.getValue().getName()) {
    					 person.getValue().addSibling(sib);
    				}
    			}
    		}
    		else if(person.getValue().getFather() != null) {
    			Set<Person> siblings = person.getValue().getFather().getChildren();
    			
    			for(Person sib : siblings) {
    				if (sib.getName() != person.getValue().getName()) {
    					 person.getValue().addSibling(sib);
    				}
    			}
    		}
    	}
    }

    public Map<String, Person> tree() {
    	return family;
    }
}