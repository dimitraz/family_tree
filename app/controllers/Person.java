package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Node used to for each member in the family tree. Contain's
 * each member's personal details.
 * 
 * @author Dimitra Zuccarelli
 */
public class Person<T extends Comparable<T>> implements Comparable<Person> {
	public Search search;
    private String name, dob;
    private char gender; 
    private Person spouse;
    private Person mother, father;
    private Set<Person> children;
    private Set<Person> siblings;
    
    public Person(String name, char gender, String dob, Person mother, Person father) {
        this.name = name;
        this.gender = gender;
        this.dob = dob;
        this.mother = mother;
        this.father = father;
        this.children = new HashSet<>();
        this.siblings = new HashSet<>();
        this.spouse = null;
    }

    @Override
    public int compareTo(Person that) {
        if(getDob().compareTo(that.getDob()) > 0) {
            return 1;
        }
        if(getDob().compareTo(that.getDob()) < 0) {
            return -1;
        }
        return 0;
    }
    
    public String toString() {
        return getName() + ", " + 
               getDob() + ", " +
    		   getGender();
    }

    /** 
     * Method to populate persons' siblings
     */
    public void populateSiblings() {
		if(this.getMother() != null) {
			Set<Person> siblings = this.getMother().getChildren();
			
			for(Person sib : siblings) {
				if (sib.getName() != this.getName()) {
					this.addSibling(sib);
				}
			}
		}
		if(this.getFather() != null) {
			Set<Person> siblings = this.getFather().getChildren();
			
			for(Person sib : siblings) {
				if (sib.getName() != this.getName()) {
					this.addSibling(sib);
				}
			}
		}
    }
    
    /**
     * Indicates whether this is a leaf node (has no children)
     * @return true if the node has no children 
     */
    public boolean isLeaf() {
        return children.isEmpty();
    }

    // Getters and setters
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public Person getSpouse() {
		return spouse;
	}

	public void setSpouse(Person spouse) {
		this.spouse = spouse;
	}

	public Person getMother() {
		return mother;
	}

	public void setMother(Person mother) {
		this.mother = mother;
	}

	public Person getFather() {
		return father;
	}

	public void setFather(Person father) {
		this.father = father;
	}

	public Set<Person> getChildren() {
		return children;
	}

	public void setChildren(Set<Person> children) {
		this.children = children;
	}

	public Set<Person> getSiblings() {
		return siblings;
	}

	public void setSiblings(Set<Person> siblings) {
		this.siblings = siblings;
	}
	
	public void addChild(Person child) {
		this.children.add(child);
	}
	
	public void addSibling(Person sib) {
		this.siblings.add(sib);
	}
	
	public void search() {
		search = new Search(this);
	}
}
