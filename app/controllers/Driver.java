package controllers;

import java.util.Scanner;
import java.util.Set;

/**
 * Basic Driver interface class to view members and modify existing
 * relationships. 
 * 
 * The application GUI (running on play 1.4) shows a much more in depth 
 * and visually appealing output.
 * 
 * @author Dimitra Zuccarelli 20072495

 */
public class Driver {
    private Scanner input;
    private static Util util;

    public Driver() {
        input = new Scanner(System.in);
    }

    /** 
     * Main method to run the family tree application
     */
    public static void main(String[] args) {
        Driver driver = new Driver();
        String path = "data/familytree.txt";
        
        // Attempt to read rules
        try {
        	util = new Util(path);
        } 
        catch (Exception e) {
            System.out.println("Unable to load file from " + path + ". Please validate file.");
            System.exit(0); // No point continuing 
        }

        driver.run(driver);
    }

    /**
     * Main menu method. Returns an integer with selected option
     * 
     * @return option number of option selected
     */
    private int menu() {
        System.out.println("-------------------------------------");
        System.out.println("Family tree"					      );
        System.out.println("-------------------------------------");
        System.out.println("  1) Search for an existing member"   );
        System.out.println("  2) Modify a member's relationships" );
        System.out.println("  3) Create a new member" 			  );
        System.out.println("  0) Exit                            ");
        System.out.print("> "                                     );

        int option = 0;
        boolean valid = false;
        do {
            try {
                option = input.nextInt();
                valid = true;
            }
            catch(Exception e) {
                input.nextLine();
                System.out.print("\nInvalid option.\nEnter a number: ");
            }
        } 
        while(!valid);
        return option;
    }

    /** 
     * Executes the main methods 
     */
    private void run(Driver driver) {
        int option = menu();

        while (option != 0) {
            switch (option) {
            case 1: 
            	input.nextLine();
            	searchPerson();
                break;
            case 2: 
            	input.nextLine();
            	Person person = returnPerson();
            	
            	if(person != null) {
            		modifyPerson(person);
            	}
                break;
            case 3:
            	input.nextLine();
            	createPerson();
            default:
                System.out.println("Invalid option selected.");
                break;
            }

            System.out.println("");
            option = menu();
        }
        System.out.println("Exiting..");
    }

    /** 
     * Method to find details about a given member. 
     */
    public void searchPerson() {
    	System.out.print("\nMember's Name: ");
    	String answer = input.nextLine();
    	
    	if(util.family.get(answer) != null) {
    		Person person = util.family.get(answer);
    		Search search = new Search(person);
    		
            System.out.println("\n-------------------------------------");
            System.out.println(person);
            System.out.println("-------------------------------------\n");
            System.out.println("Please use the application to view all extended family relationships visually.");
            
            if(person.getMother() != null)
            	System.out.println("\nMother: " + person.getMother());
            
            if(person.getFather() != null)
            	System.out.println("\nFather: " + person.getFather());
        	
            if((search.siblings().size() != 0)) {
            	System.out.println("\nSiblings: ");
            	for(Person sib : search.siblings()) {
            		System.out.println(sib);
            	}
            }
        	
        	if(person.getSpouse() != null)
            	System.out.println("\nSpouse: " + person.getSpouse());
        	
            if((search.children().size() != 0)) {
            	System.out.println("\nChildren: ");
            	for(Person child : search.children()) {
            		System.out.println(child);
            	}
            }
    	}
    	else {
    		System.out.print("\nNo family member found with that name.\n");
    	}
    }
    
    /** 
     * Method to return a member
     */
    public Person returnPerson() {
    	System.out.print("\nMember's Name: ");
    	String answer = input.nextLine();
    	
    	if(util.family.get(answer) != null) {
    		return util.family.get(answer);
    	}
    	else {
    		System.out.println("\nNo family member found with that name.");
    		return null;
    	}    	
    }
    
    /**
     * Method to modify relationships between family members
     * Allows the user to add/modify existing sibling, spouse, parent and children
     * relationships
     */
    public void modifyPerson(Person person) {
    	Search search = new Search(person);
    	
    	System.out.println();
        System.out.println("-------------------------------------");
        System.out.println("Modify relationships"			      );
        System.out.println("-------------------------------------");
        System.out.println("What would you like to do?\n"	      );
        System.out.println("  1) Add/modify sibling relation"     );
        System.out.println("  2) Add/modify spouse"     		  );
        System.out.println("  3) Add/modify parents"     		  );
        System.out.println("  4) Add/modify children"     		  );
        System.out.print("> "                                     );
    	
    	String option = input.nextLine();
    	
    	switch (option) {
        case "1": 
        	// Display siblings
        	if(search.siblings().size() != 0) {
        		System.out.println("\n" + person.getName() + "'s siblings:");
        		for(Person sib : search.siblings()) {
        			System.out.println(sib);
        		}
        	}
        	else {
        		System.out.println(person.getName() + " has no siblings");
        	}
        	
        	// Call method to modify/add siblings
        	modifySiblings(person);
            break;
        case "2": 
        	// Display spouse
        	if(person.getSpouse() != null) {
        		System.out.println("\n" + person.getName() + "'s spouse is: " + person.getSpouse());
        	}
        	else {
        		System.out.println("\n" + person.getName() + " is not married.");
        	}
        	
        	// Call method to modify/add spouse
        	modifySpouse(person);
            break;
        case "3": 
        	// Display parents
        	System.out.println();
        	if(person.getMother() != null) {
        		System.out.println(person.getName() + "'s mother is: " + person.getMother());
        	}
        	if(person.getFather() != null) {
        		System.out.println(person.getName() + "'s father is: " + person.getFather());
        	}
        	else if(person.getFather() == null && person.getMother() == null) {
        		System.out.println(person.getName() + " does not have parents.");
        	}
        
        	// Call method to modify/add parents
        	modifyParents(person);
            break;
        case "4": 
        	// Display children
           	if(search.children().size() != 0) {
        		System.out.println("\n" + person.getName() + "'s children:");
        		for(Person child : search.children()) {
        			System.out.println(child);
        		}
        	}
        	else {
        		System.out.println(person.getName() + " has no children.");
        	}
           	
           	// Call method to modify/add parents
        	modifyChildren(person);
            break;
        default:
            System.out.println("Invalid option selected.");
            break;
        }

    } 
    
    /**
     * Method to add a new sibling and create a sibling relation 
     * or create a sibling relation between `person` and an existing
     * member
     */
    public void modifySiblings(Person person) {
    	// Add siblings
    	System.out.print("\nDo you wish to add a sibling? (yes/no) ");
    	String answer = input.nextLine();
    	
    	if(answer.equals("yes") || (answer.equals("y"))) {
    		System.out.print("\nSibling's Name: ");
    		String name = input.nextLine(); 
    		
    		Person sibling;
    		if(util.family.get(name) != null) {
    			sibling = util.family.get(name);
    		}
    		else {
        		System.out.print("Sibling's Dob: ");
        		String dob = input.nextLine();
        		
        		System.out.print("Sibling's Gender: ");
        		String gender = input.nextLine();
        		
    			sibling = new Person(name, gender.toUpperCase().charAt(0), dob, person.getMother(), person.getFather());
    			util.family.put(name, sibling);
    		}
    		
			person.addSibling(sibling);
			sibling.addSibling(person);
			
			System.out.println("\nSibling: " + sibling.getName() + " successfully added.");
    	}
    }
    
    /**
     * Method to add a new spouse and create a relationship 
     * or marry `person` and an existing member.
     */
    public void modifySpouse(Person person) {
    	// Add or replace current spouse
    	System.out.print("\nDo you wish to add/modify " + person.getName() + "'s spouse? (yes/no) ");
    	String answer = input.nextLine();
    	
    	if(answer.equals("yes") || (answer.equals("y"))) {
    		System.out.print("\nSpouse's Name: ");
    		String name = input.nextLine(); 
    		
    		Person spouse;
    		if(util.family.get(name) != null) {
    			spouse = util.family.get(name);
    		}
    		else {
        		System.out.print("Spouse's Dob: ");
        		String dob = input.nextLine();
        		
        		System.out.print("Spouse's Gender: ");
        		String gender = input.nextLine();
        		
    			spouse = new Person(name, gender.toUpperCase().charAt(0), dob, person.getMother(), person.getFather());
    			util.family.put(name, spouse);     	
    		}
    		
			person.setSpouse(spouse);
			spouse.setSpouse(person);
			System.out.println("\nSpouse: " + spouse.getName() + " successfully added.");
    	}
    }
    
    /**
     * Method to add new parent objects and create a relation 
     * or create a new parental relation between `person` and an existing
     * member
     */
    public void modifyParents(Person person) {
    	// Add/modify parents
    	System.out.print("\nDo you wish to add/modify " + person.getName() + "'s parents? (yes/no) ");
    	String answer = input.nextLine();
    	
    	if(answer.equals("yes") || (answer.equals("y"))) {
    		System.out.print("\nMother's Name: ");
    		String mothername = input.nextLine(); 
    		
    		Person mother;
    		if(util.family.get(mothername) != null) {
    			mother = util.family.get(mothername);
    		}
    		else {
        		System.out.print("Mother's Dob: ");
        		String motherdob = input.nextLine();
        		
    			mother = new Person(mothername, 'F', motherdob, null, null);
    			util.family.put(mothername, mother);
    		}
    		
    		// Add the mother
			person.setMother(mother);
			mother.addChild(person);
			System.out.println("\nMother: " + mother.getName() + " successfully added.");
    		
			// Add the father
    		System.out.print("\nFather's Name: ");
    		String fathername = input.nextLine(); 
    		
    		Person father;
    		if(util.family.get(fathername) != null) {
    			father = util.family.get(fathername);
    		}
    		else {
        		System.out.print("Father's Dob: ");
        		String fatherdob = input.nextLine();
        		
    			father = new Person(fathername, 'F', fatherdob, null, null);
    			util.family.put(fathername, father);
    		}
    		
    		// Add the father
			person.setFather(father);
			father.addChild(person);
			
			// Populate siblings
	    	person.populateSiblings();
	    	
			System.out.println("\nFather: " + father.getName() + " successfully added.");
    	}	
    }
    
    /**
     * Method to add a new child object and create a relation 
     * or create a parent-child relation between `person` and an existing
     * member
     */
    public void modifyChildren(Person person) {
    	System.out.print("\nDo you wish to add/modify " + person.getName() + "'s children? (yes/no) ");
    	String answer = input.nextLine();
    	
    	if(answer.equals("yes") || (answer.equals("y"))) {
    		System.out.print("\nChild's Name: ");
    		String name = input.nextLine(); 
    		
    		Person child;
    		if(util.family.get(name) != null) {
    			child = util.family.get(name);
    		}
    		else {
        		System.out.print("Child's Dob: ");
        		String dob = input.nextLine();
        		
        		System.out.print("Child's Gender: ");
        		String gender = input.nextLine();
        		
        		// Create new person object
        		if(person.getGender() == 'F')
        			child = new Person(name, gender.toUpperCase().charAt(0), dob, person, person.getSpouse());
        		else
        			child = new Person(name, gender.toUpperCase().charAt(0), dob, person.getSpouse(), person);
        		
        		// Add the child to the family tree map
        		util.family.put(name, child);
    		}
    		
    		// Add the child to the parent
    		person.addChild(child);
			
			// Set mother and father
			if(person.getGender() == 'F') {
				child.setMother(person);
				
				if(person.getSpouse() != null) {
					child.setFather(person);
				}
			}
			else if(person.getGender() == 'M') {
				child.setFather(person);
				
				if(person.getSpouse() != null) {
					child.setMother(person);
				}
			}
			
			// Populate child's siblings
			child.populateSiblings();
			
			System.out.print("\nChild: " + child + " successfully added.\n");
    	}
    }
    
    /**
     * Method to create a new person and add them to the family tree.
     */
    public void createPerson() {
        System.out.println("-------------------------------------");
        System.out.println("Create a new member"			      );
        System.out.println("-------------------------------------");
        
      	System.out.print("Member's name: ");
    	String name = input.nextLine();
    	
    	if(util.family.get(name) == null) {
    	
	    	System.out.print("Member's dob: ");
	    	String dob = input.nextLine();
	    	
	    	System.out.print("Member's gender: ");
	    	String gender = input.nextLine();
	    	
	    	System.out.print("Member's father's name (press enter if null): ");
	    	String father = input.nextLine();
	    	
	    	System.out.print("Member's mother's name (press enter if null): ");
	    	String mother = input.nextLine();
	    	
	    	System.out.print("Member's spouse's name (press enter if null): ");
	    	String spouse = input.nextLine();
	    	
	    	Person person = new Person(name, gender.toUpperCase().charAt(0), dob, util.family.get(mother), util.family.get(father));
	    	person.setSpouse(util.family.get(spouse));
	    	
	    	// Set father
	    	if(util.family.get(father) != null) {
	    		util.family.get(father).addChild(person);
	    	}
	    	
	    	// Set mother
	    	if(util.family.get(mother) != null) {
	    		util.family.get(mother).addChild(person);
	    	}
	    	
	    	// Populate siblings
	    	person.populateSiblings();
	    	
	    	// Add the person to the family map
	    	util.family.put(name, person);
	    	
	    	System.out.println("\nPerson successfully created.");
    	}
    	else {
    		System.out.println("\nThat person already exists.");
    	}
    }
    
}
