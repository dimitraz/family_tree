package controllers;

import play.*;
import play.mvc.*;

import java.io.File;
import java.util.*;
import models.*;

/**
 * Controller used to generate the family tree. Corresponds
 * to the family tree view.
 * 
 * @author Dimitra Zuccarelli
 *
 */
public class FamilyTree extends Controller {
	Search search, search_sp;
	static Util util;
	static Map<String, Person> tree = tree();
	
	public static Map<String, Person> tree() {
		util = null;
		try {
			util = new Util("data/familytree.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return util.tree();
	}
	
	public static void index() {
		FamilyTree driver = new FamilyTree();

		// Avery & Victor are good members to look at
		Person person = driver.tree.get(params.get("user"));
		driver.search = new Search(person);
		
		if(person.getSpouse() != null) {
			driver.search_sp = new Search(person.getSpouse());
		}
		
		render(person, driver, driver.search, driver.search_sp);
	}
}
