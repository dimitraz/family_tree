package controllers;

import play.*;
import play.mvc.*;

import java.io.File;
import java.util.*;
import models.*;

public class Home extends Controller {
	Util util;

	public static void index() {
		Collection<Person> members = FamilyTree.tree.values();
		render(members);
	}
	
}
