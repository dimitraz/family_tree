package controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Search class is a utility to easily search for a 
 * member's relationships. 
 * 
 * The prefix 'p' stands for paternal; The prefix 'm' stands
 * for maternal.
 * 
 * @author Dimitra Zuccarelli
 *
 */
public class Search {
	Person person;
	
	public Search(Person person) {
		this.person = person;
	}

	public Set<Person> pcousins() {
		Set<Person> cousins = new HashSet<>();
		
		if(person.getFather() != null) {
			Person father = person.getFather();
			
			Set<Person> siblings = father.getSiblings(); // Father's siblings
			
			for(Person sib : siblings) {
				cousins.addAll(sib.getChildren());
			}
		}
		
		return cousins;
	}
	
	public Set<Person> mcousins() {
		Set<Person> cousins = new HashSet<>();
		
		if(person.getMother() != null) {
			Person mother = person.getMother();
			
			Set<Person> siblings = mother.getSiblings(); // Mother's siblings
			
			for(Person sib : siblings) {
				cousins.addAll(sib.getChildren());
			}
		}
		
		return cousins;
	}
	
	public Set<Person> pgrandparents() {
		Set<Person> grandparents = new HashSet<>();
		
		if(person.getFather() != null) {
			if(person.getFather().getFather() != null) {
				grandparents.add(person.getFather().getFather());
			} else {
				grandparents.add(new Person("?", 'M', "", null, null));
			}
			
			if(person.getFather().getMother() != null) {
				grandparents.add(person.getFather().getMother());
				
			} else {
				grandparents.add(new Person("?", 'F', "", null, null));
			}
		}
		else {
			grandparents.add(new Person("?", 'M', "", null, null));
			grandparents.add(new Person("?", 'F', "", null, null));
		}
		
		return grandparents;		
	}
	
	public Set<Person> mgrandparents() {
		Set<Person> grandparents = new HashSet<>();
		
		if(person.getMother() != null) {
			if(person.getMother().getFather() != null) {
				grandparents.add(person.getMother().getFather());
			} else {
				grandparents.add(new Person("?", 'M', "", null, null));
			}
			
			if(person.getMother().getMother() != null) {
				grandparents.add(person.getMother().getMother());
			} else {
				grandparents.add(new Person("?", 'F', "", null, null));
			}
		}
		else {
			grandparents.add(new Person("?", 'M', "", null, null));
			grandparents.add(new Person("?", 'F', "", null, null));
		}
		return grandparents;
	}
	
	public Set<Person> siblings() {
		return person.getSiblings();
	}
	
	public Set<Person> children() {
		return person.getChildren();
	}
	
	public Set<Person> puncles() {
		Set<Person> uncles = new HashSet<>();
		
		if(person.getFather() != null) {
			Set<Person> siblings = person.getFather().getSiblings();
			
			for(Person sib : siblings) {
				if (sib.getGender() == 'M') {
					uncles.add(sib);
				}
			}
		}
		
		return uncles;
	}
	
	public Set<Person> muncles() {
		Set<Person> uncles = new HashSet<>();
		
		if(person.getMother() != null) {
			Set<Person> siblings = person.getMother().getSiblings();
			
			for(Person sib : siblings) {
				if (sib.getGender() == 'M') {
					uncles.add(sib);
				}
			}
		}
		
		return uncles;		
	}
	
	public Set<Person> paunts() {
		Set<Person> aunts = new HashSet<>();
		
		if(person.getFather() != null) {
			Set<Person> siblings = person.getFather().getSiblings();
			
			for(Person sib : siblings) {
				if (sib.getGender() == 'F') {
					aunts.add(sib);
				}
			}
		}
		
		return aunts;			
	}
	
	public Set<Person> maunts() {
		Set<Person> aunts = new HashSet<>();
		
		if(person.getMother() != null) {
			Set<Person> siblings = person.getMother().getSiblings();
			
			for(Person sib : siblings) {
				if (sib.getGender() == 'F') {
					aunts.add(sib);
				}
			}
		}
		
		return aunts;			
	}
	
	public Person spouse() {
		return person.getSpouse();
	}
}
